# Why?

If your are working with Dutch Datex-2 data, you will find that most locations are indicated by their AlertC location code.

In most other parts of Europe these refer to the Location Code List (LCL), the same which is also used for location references in Traffic Message Channel (TMC). But the Netherlands have decided to use a separate location table for Datex-2, known as [Verkeers Informatie Locatie Database (VILD)](https://docs.ndw.nu/documenten/#vild). The VILD differs from the LCL in several ways:

* It covers locations which are not in the LCL.
* It has references to kilometric points, which the Location Table Exchange Format (LTEF) does not support
* It describes boundaries of administrative (and other) areas, which LTEF also does not support
* It uses different location codes than the LCL—VILD codes use numbers below ~30000, whereas the LCL uses codes above that. This means you can’t just use the LCL instead of the VILD, or vice versa.
* It is distributed in a different format: while the LCL is spread out over a handful of tables, the VILD puts (almost) everything into one table.

The latter makes your life a lot easier if you are only working with the VILD, but can be a headache if you already have the infrastructure for LCL processing in place (such as traffxml/traff-libalertclocation>).

# What does this do?

vild2ltef converts the VILD into something close to the LTEF, which traffxml/traff-libalertclocation> can work with.

A word of caution: the output is not 100% compliant LTEF, as not all tables required by LTEF get generated. However, the resulting set of tables should work for most purposes, including those of traffxml/traff-libalertclocation>.

Refer to the wiki for usage instructions.