create table LOCATIONDATASETS(CID integer, TABCD integer, DCOMMENT text, VERSION text, VERSIONDESCRIPTION text);
insert into locationdatasets(cid, tabcd, dcomment, version, versiondescription) select 39 as cid, 520 as tabcd, null as dcomment, firstname as version, secndname as versiondescription from vild where locnr = 0;
create table COUNTRIES(CID integer, ECC text, CCD text, CNAME text);
insert into countries(cid, ecc, ccd, cname) values (39,'E3','8','Netherlands');
--delete from vild where locnr = 0;
create table tmpnames(name text);
insert into tmpnames select distinct firstname from vild where locnr != 0;
insert into tmpnames select distinct secndname from vild where locnr != 0;
insert into tmpnames select distinct roadname from vild where locnr != 0;
create table NAMES(CID integer, LID integer, NID integer primary key autoincrement, NAME text, NCOMMENT text, OFFICIALNAME text);
create index names_name on names(name);
insert into names(cid, lid, name, ncomment, officialname) select distinct 39 as cid, 1 as lid, name, null as ncomment, null as officialname from tmpnames;
drop table tmpnames;
create table POFFSETS as select 39 as CID, 520 as TABCD, locnr as LCD, case when negoff = 0 then null else negoff end as NEG_OFF_LCD, case when posoff = 0 then null else posoff end as POS_OFF_LCD from vild where loctype like 'P%';
create table SOFFSETS as select 39 as CID, 520 as TABCD, locnr as LCD, case when negoff = 0 then null else negoff end as NEG_OFF_LCD, case when posoff = 0 then null else posoff end as POS_OFF_LCD from vild where loctype in ('L3.0', 'L4.0');
create table tmpareas(CID integer, TABCD integer, LCD integer primary key, CLASS text, TCD integer, STCD integer, NID integer, POL_LCD integer);
create index tmpareas_tcd on tmpareas(tcd);
insert into tmpareas(cid, tabcd, lcd, class, tcd, stcd, nid, pol_lcd) select 39 as cid, 520 as tabcd, locnr as lcd, substr(loctype, 1, 1) as class, substr(loctype, 2, instr(loctype, '.') - 2) as tcd, substr(loctype, instr(loctype, '.') + 1) as stcd, (select nid from names where names.name = vild.firstname) as nid, case when arearef = 0 then null else arearef end as pol_lcd from vild where loctype like 'A%';
create table ADMINISTRATIVEAREAS as select * from tmpareas where tcd in (1, 2, 3, 7, 8, 9, 10, 11);
create table OTHERAREAS as select * from tmpareas where tcd not in (1, 2, 3, 7, 8, 9, 10, 11);
-- when pol_lcd refers to a non-admin area, replace it with the pol_lcd of its parent until we have an admin area
update otherareas set pol_lcd = (select pol_lcd from tmpareas where tmpareas.lcd = otherareas.pol_lcd) where pol_lcd in (select lcd from otherareas);
drop index tmpareas_tcd;
drop table tmpareas;
create table tmplines(CID integer, TABCD integer, LCD integer primary key, CLASS text, TCD integer, STCD integer, ROADNUMBER text, RNID integer, N1ID integer, N2ID integer, ROA_LCD integer, SEG_LCD integer, POL_LCD integer, PES_LEV integer, RDID integer);
create index tmplines_tcd on tmplines(tcd);
insert into tmplines select 39 as cid, 520 as tabcd, locnr as lcd, substr(loctype, 1, 1) as class, substr(loctype, 2, instr(loctype, '.') - 2) as tcd, substr(loctype, instr(loctype, '.') + 1) as stcd, roadnumber, (select nid from names where names.name = vild.roadname) as rnid, (select nid from names where names.name = vild.firstname) as n1id, (select nid from names where names.name = vild.secndname) as n2id, case when linref = 0 then null else linref end as roa_lcd, case when linref = 0 then null else linref end as seg_lcd, arearef as pol_lcd, case when loctype in ('L1.1', 'L6.1') then 1 else 2 end as pes_lev, null as rdid from vild where loctype like 'L%';
-- when pol_lcd refers to a non-admin area, replace it with the pol_lcd of its parent until we have an admin area
update tmplines set pol_lcd = (select pol_lcd from otherareas where otherareas.lcd = tmplines.pol_lcd) where pol_lcd in (select lcd from otherareas);
create table ROADS as select CID, TABCD, LCD, CLASS, TCD, STCD, ROADNUMBER, RNID, N1ID, N2ID, POL_LCD, PES_LEV, RDID from tmplines where tcd not in (3, 4);
-- when seg_lcd refers to a road, set it to null
update tmplines set seg_lcd = null where seg_lcd in (select lcd from roads);
create table SEGMENTS as select CID, TABCD, LCD, CLASS, TCD, STCD, ROADNUMBER, RNID, N1ID, N2ID, ROA_LCD, SEG_LCD, POL_LCD, RDID from tmplines where tcd in (3, 4);
-- when roa_lcd refers to a segment, replace it with the roa_lcd of its parent until we have a road
update segments set roa_lcd = (select roa_lcd from tmplines where tmplines.lcd = segments.roa_lcd) where roa_lcd in (select lcd from segments);
drop index tmplines_tcd;
drop table tmplines;
create table POINTS(CID integer, TABCD integer, LCD integer primary key, CLASS text, TCD integer, STCD integer, JUNCTIONNUMBER text, RNID integer, N1ID integer, N2ID integer, POL_LCD integer, OTH_LCD integer, SEG_LCD integer, ROA_LCD integer, INPOS integer, INNEG integer, OUTPOS integer, OUTNEG integer, PRESENTPOS integer, PRESENTNEG integer, DIVERSIONPOS text, DIVERSIONNEG text, XCOORD text, YCOORD text, INTERRUPTSROAD integer, URBAN integer, JNID integer);
insert into points select 39 as cid, 520 as tabcd, locnr as lcd, substr(loctype, 1, 1) as class, substr(loctype, 2, instr(loctype, '.') - 2) as tcd, substr(loctype, instr(loctype, '.') + 1) as stcd, case when junctref = 0 then null else junctref end as junctionnumber, (select nid from names where names.name = vild.roadname) as rnid, (select nid from names where names.name = vild.firstname) as n1id, (select nid from names where names.name = vild.secndname) as n2id, arearef as pol_lcd, arearef as oth_lcd, case when linref = 0 then null else linref end as seg_lcd, case when linref = 0 then null else linref end as roa_lcd, posin as inpos, negin as inneg, posout as outpos, negout as outneg, prespos as presentpos, presneg as presentneg, null as diversionpos, null as diversionneg, printf('%+09d', round(x * 100000)) as xcoord, printf('%+08d', round(y * 100000)) as ycoord, null as interruptsroad, 0 as urban, null as jnid from vild, wgs84 where locnr = loc_nr and loctype like 'P%';
-- when oth_lcd refers to an admin area, set it to null
update points set oth_lcd = null where oth_lcd in (select lcd from administrativeareas);
-- when pol_lcd refers to a non-admin area, replace it with the pol_lcd of its parent until we have an admin area
update points set pol_lcd = (select pol_lcd from otherareas where otherareas.lcd = points.pol_lcd) where pol_lcd in (select lcd from otherareas);
-- when seg_lcd refers to a road, set it to null
update points set seg_lcd = null where seg_lcd in (select lcd from roads);
-- when roa_lcd refers to a segment, replace it with the roa_lcd of its parent until we have a road
update points set roa_lcd = (select roa_lcd from segments where segments.lcd = points.roa_lcd) where roa_lcd in (select lcd from segments);

